import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { writeFileSync } from 'fs';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Pokémons API')
    .setDescription('Pokémons API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  writeFileSync('../swagger.json', JSON.stringify(document, null, 2), 'utf8');
  SwaggerModule.setup('docs', app, document);
  app.enableCors();
  await app.listen(8443);
}
bootstrap();
