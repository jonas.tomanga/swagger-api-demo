import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
export class Pokemon {
  @ApiProperty()
  id: number;
  @ApiProperty()
  name: string;
  @ApiProperty()
  type: string;
  @ApiProperty()
  description: string;
  @ApiPropertyOptional()
  image?: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;

  constructor(data: Pokemon) {
    Object.assign(this, data);
  }
}
