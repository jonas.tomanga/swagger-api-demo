import { ApiProperty } from '@nestjs/swagger';
export class CreatePokemonDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  type: string;
  @ApiProperty()
  description: string;
  @ApiProperty()
  image?: string;
}
