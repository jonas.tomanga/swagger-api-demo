import { Pokemon } from './entities/pokemon.entity';

export const POKEMONS: Pokemon[] = [
  new Pokemon({
    id: 1,
    name: 'Bulbasaur',
    type: 'Grass/Poison',
    image: 'https://img.pokemondb.net/artwork/bulbasaur.jpg',
    description:
      "Bulbasaur can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun's rays, the seed grows progressively larger.",
    createdAt: new Date(),
    updatedAt: new Date(),
  }),
  new Pokemon({
    id: 4,
    name: 'Charmander',
    type: 'Fire',
    image: 'https://img.pokemondb.net/artwork/charmander.jpg',
    description:
      'It has a preference for hot things. When it rains, steam is said to spout from the tip of its tail.',
    createdAt: new Date(),
    updatedAt: new Date(),
  }),
  new Pokemon({
    id: 2,
    name: 'Ivysaur',
    type: 'Grass/Poison',
    image: 'https://img.pokemondb.net/artwork/ivysaur.jpg',
    description:
      'When the bulb on its back grows large, it appears to lose the ability to stand on its hind legs.',
    createdAt: new Date(),
    updatedAt: new Date(),
  }),
];
