import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PokemonsService } from './pokemons.service';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { Pokemon } from './entities/pokemon.entity';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('pokemons')
@ApiTags('pokemons')
export class PokemonsController {
  constructor(private readonly pokemonsService: PokemonsService) {}

  @Post()
  @ApiOperation({ summary: 'register a pokemon' })
  @ApiResponse({
    status: 201,
    description: 'The pokemon has been created.',
    type: Pokemon,
  })
  create(@Body() createPokemonDto: CreatePokemonDto): Pokemon {
    return this.pokemonsService.create(createPokemonDto);
  }

  @Get()
  @ApiOperation({ summary: 'get all pokemons' })
  @ApiResponse({
    status: 200,
    description: 'Return all pokemons.',
    type: Pokemon,
    isArray: true,
  })
  findAll(): Pokemon[] {
    return this.pokemonsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Pokemon {
    return this.pokemonsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePokemonDto: UpdatePokemonDto,
  ): Pokemon {
    return this.pokemonsService.update(+id, updatePokemonDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): void {
    return this.pokemonsService.remove(Number(id));
  }
}
