import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { Pokemon } from './entities/pokemon.entity';
import { POKEMONS } from './pokemons';

@Injectable()
export class PokemonsService {
  create(createPokemonDto: CreatePokemonDto): Pokemon {
    return new Pokemon({
      id: 5,
      ...createPokemonDto,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  findAll(): Pokemon[] {
    return POKEMONS;
  }

  findOne(id: number): Pokemon {
    const pokemon = POKEMONS.find((pokemon) => pokemon.id === id);
    if (!pokemon) {
      throw new NotFoundException(`Pokemon with id ${id} not found`);
    }
    return pokemon;
  }

  update(id: number, updatePokemonDto: UpdatePokemonDto): Pokemon {
    const pokemon = POKEMONS.find((pokemon) => pokemon.id === id);
    if (!pokemon) {
      throw new NotFoundException(`Pokemon with id ${id} not found`);
    }
    return { ...pokemon, ...updatePokemonDto };
  }

  remove(id: number) {
    return;
  }
}
