import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { pokemonsService } from "./services/pokemon.service";
import { Pokemon } from "./api";

function App() {
  const [pokemons, setPokemons] = React.useState<Pokemon[]>([]);
  const [isLoading, setLoading] = React.useState(false);
  const [error, setError] = React.useState<unknown>(null);

  React.useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        // TODO: fetch data from API
        const results = await pokemonsService.pokemonsControllerFindAll();
        setTimeout(() => {
          setPokemons(results.data);
          setLoading(false);
        }, 1000);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <>
          <h1>Welcome to the pokédex</h1>
          {isLoading && <img src={logo} className="App-logo" alt="logo" />}

          {!isLoading && !error && pokemons.length > 0 && (
            <div className="pokedex">
              {pokemons.map((pokemon) => (
                <div key={pokemon.id} className="pokemon">
                  <div
                    className="pokemon-image"
                    style={{
                      backgroundImage: `url(${pokemon.image})`,
                      height: "200px",
                      position: "relative",
                    }}
                  >
                    <div
                      style={{
                        backgroundColor: "rgba(0,0,0,0.6)",
                        position: "absolute",
                        bottom: 0,
                        width: "100%",
                      }}
                    >
                      <h2 style={{ marginBottom: 0, marginTop: 0 }}>
                        {pokemon.name}
                      </h2>
                    </div>
                  </div>
                  <p>
                    <strong>Description:</strong> {pokemon.description}
                  </p>
                  <p>
                    <strong>Type:</strong> {pokemon.type}
                  </p>
                </div>
              ))}
            </div>
          )}
        </>
      </header>
    </div>
  );
}

export default App;
