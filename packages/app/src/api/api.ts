/* tslint:disable */
/* eslint-disable */
/**
 * Pokémons API
 * Pokémons API
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import { Configuration } from './configuration';
import globalAxios, { AxiosPromise, AxiosInstance, AxiosRequestConfig } from 'axios';
// Some imports not used depending on template conditions
// @ts-ignore
import { DUMMY_BASE_URL, assertParamExists, setApiKeyToObject, setBasicAuthToObject, setBearerAuthToObject, setOAuthToObject, setSearchParams, serializeDataIfNeeded, toPathString, createRequestFunction } from './common';
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from './base';

/**
 * 
 * @export
 * @interface CreatePokemonDto
 */
export interface CreatePokemonDto {
    /**
     * 
     * @type {string}
     * @memberof CreatePokemonDto
     */
    'name': string;
    /**
     * 
     * @type {string}
     * @memberof CreatePokemonDto
     */
    'type': string;
    /**
     * 
     * @type {string}
     * @memberof CreatePokemonDto
     */
    'description': string;
    /**
     * 
     * @type {string}
     * @memberof CreatePokemonDto
     */
    'image': string;
}
/**
 * 
 * @export
 * @interface Pokemon
 */
export interface Pokemon {
    /**
     * 
     * @type {number}
     * @memberof Pokemon
     */
    'id': number;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'name': string;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'type': string;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'description': string;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'image'?: string;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'createdAt': string;
    /**
     * 
     * @type {string}
     * @memberof Pokemon
     */
    'updatedAt': string;
}

/**
 * DefaultApi - axios parameter creator
 * @export
 */
export const DefaultApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        appControllerGetHello: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * DefaultApi - functional programming interface
 * @export
 */
export const DefaultApiFp = function(configuration?: Configuration) {
    const localVarAxiosParamCreator = DefaultApiAxiosParamCreator(configuration)
    return {
        /**
         * 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async appControllerGetHello(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.appControllerGetHello(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
    }
};

/**
 * DefaultApi - factory interface
 * @export
 */
export const DefaultApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    const localVarFp = DefaultApiFp(configuration)
    return {
        /**
         * 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        appControllerGetHello(options?: any): AxiosPromise<void> {
            return localVarFp.appControllerGetHello(options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * DefaultApi - object-oriented interface
 * @export
 * @class DefaultApi
 * @extends {BaseAPI}
 */
export class DefaultApi extends BaseAPI {
    /**
     * 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public appControllerGetHello(options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).appControllerGetHello(options).then((request) => request(this.axios, this.basePath));
    }
}


/**
 * PokemonsApi - axios parameter creator
 * @export
 */
export const PokemonsApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @summary register a pokemon
         * @param {CreatePokemonDto} createPokemonDto 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerCreate: async (createPokemonDto: CreatePokemonDto, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'createPokemonDto' is not null or undefined
            assertParamExists('pokemonsControllerCreate', 'createPokemonDto', createPokemonDto)
            const localVarPath = `/pokemons`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(createPokemonDto, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary get all pokemons
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerFindAll: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/pokemons`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerFindOne: async (id: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'id' is not null or undefined
            assertParamExists('pokemonsControllerFindOne', 'id', id)
            const localVarPath = `/pokemons/{id}`
                .replace(`{${"id"}}`, encodeURIComponent(String(id)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerRemove: async (id: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'id' is not null or undefined
            assertParamExists('pokemonsControllerRemove', 'id', id)
            const localVarPath = `/pokemons/{id}`
                .replace(`{${"id"}}`, encodeURIComponent(String(id)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'DELETE', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @param {string} id 
         * @param {object} body 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerUpdate: async (id: string, body: object, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'id' is not null or undefined
            assertParamExists('pokemonsControllerUpdate', 'id', id)
            // verify required parameter 'body' is not null or undefined
            assertParamExists('pokemonsControllerUpdate', 'body', body)
            const localVarPath = `/pokemons/{id}`
                .replace(`{${"id"}}`, encodeURIComponent(String(id)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'PATCH', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(body, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * PokemonsApi - functional programming interface
 * @export
 */
export const PokemonsApiFp = function(configuration?: Configuration) {
    const localVarAxiosParamCreator = PokemonsApiAxiosParamCreator(configuration)
    return {
        /**
         * 
         * @summary register a pokemon
         * @param {CreatePokemonDto} createPokemonDto 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async pokemonsControllerCreate(createPokemonDto: CreatePokemonDto, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Pokemon>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.pokemonsControllerCreate(createPokemonDto, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @summary get all pokemons
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async pokemonsControllerFindAll(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Array<Pokemon>>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.pokemonsControllerFindAll(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async pokemonsControllerFindOne(id: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.pokemonsControllerFindOne(id, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async pokemonsControllerRemove(id: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.pokemonsControllerRemove(id, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @param {string} id 
         * @param {object} body 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async pokemonsControllerUpdate(id: string, body: object, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.pokemonsControllerUpdate(id, body, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
    }
};

/**
 * PokemonsApi - factory interface
 * @export
 */
export const PokemonsApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    const localVarFp = PokemonsApiFp(configuration)
    return {
        /**
         * 
         * @summary register a pokemon
         * @param {CreatePokemonDto} createPokemonDto 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerCreate(createPokemonDto: CreatePokemonDto, options?: any): AxiosPromise<Pokemon> {
            return localVarFp.pokemonsControllerCreate(createPokemonDto, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary get all pokemons
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerFindAll(options?: any): AxiosPromise<Array<Pokemon>> {
            return localVarFp.pokemonsControllerFindAll(options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerFindOne(id: string, options?: any): AxiosPromise<void> {
            return localVarFp.pokemonsControllerFindOne(id, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @param {string} id 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerRemove(id: string, options?: any): AxiosPromise<void> {
            return localVarFp.pokemonsControllerRemove(id, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @param {string} id 
         * @param {object} body 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        pokemonsControllerUpdate(id: string, body: object, options?: any): AxiosPromise<void> {
            return localVarFp.pokemonsControllerUpdate(id, body, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * PokemonsApi - object-oriented interface
 * @export
 * @class PokemonsApi
 * @extends {BaseAPI}
 */
export class PokemonsApi extends BaseAPI {
    /**
     * 
     * @summary register a pokemon
     * @param {CreatePokemonDto} createPokemonDto 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PokemonsApi
     */
    public pokemonsControllerCreate(createPokemonDto: CreatePokemonDto, options?: AxiosRequestConfig) {
        return PokemonsApiFp(this.configuration).pokemonsControllerCreate(createPokemonDto, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary get all pokemons
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PokemonsApi
     */
    public pokemonsControllerFindAll(options?: AxiosRequestConfig) {
        return PokemonsApiFp(this.configuration).pokemonsControllerFindAll(options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @param {string} id 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PokemonsApi
     */
    public pokemonsControllerFindOne(id: string, options?: AxiosRequestConfig) {
        return PokemonsApiFp(this.configuration).pokemonsControllerFindOne(id, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @param {string} id 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PokemonsApi
     */
    public pokemonsControllerRemove(id: string, options?: AxiosRequestConfig) {
        return PokemonsApiFp(this.configuration).pokemonsControllerRemove(id, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @param {string} id 
     * @param {object} body 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof PokemonsApi
     */
    public pokemonsControllerUpdate(id: string, body: object, options?: AxiosRequestConfig) {
        return PokemonsApiFp(this.configuration).pokemonsControllerUpdate(id, body, options).then((request) => request(this.axios, this.basePath));
    }
}


